let score = 0;
let difficulty;
function randomize(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

/* Classes */
class Game {
    constructor (difficulty) {
        this.difficulty = difficulty;
        this.score = score;
        this.generatedRandomsArray = [];
    }

    checkRepeats (randomInstances) {
        if (this.generatedRandomsArray.includes(randomInstances)) {
            randomInstances = randomize(100);
            return this.checkRepeats(randomInstances);
        } else {
            this.generatedRandomsArray.push(randomInstances);
            return randomInstances;
        }
    }
}

class Player {
    constructor (score) {
        this._score = score;
    }
    get score () {
        return this._score;
    }
    set score (value) {
        this._score = value;
    }
}


let humanScoreCount = document.getElementById('human-score-count');
let AIScoreCount = document.getElementById('AI-score-count');


document.getElementById('launch-game').addEventListener('click', () => {
    location.reload();
}, true);



/* Launch new game */
window.onload = modalGameStart;

function modalGameStart() {
    document.querySelectorAll('.modal-start')[0].style.display = 'flex';

    [].forEach.call(document.querySelectorAll('.difficulties-buttons-item'), function (item) {
        item.addEventListener('click', ()=> {
            document.querySelectorAll('.modal-start')[0].style.display = 'none';
            switch (item.id) {
                case 'easy':
                    difficulty = 1500;
                    break;
                case 'normal':
                    difficulty = 1000;
                    break;
                case 'difficult':
                    difficulty = 500;
                    break;
            }
            launch();
        });
    });
}

function modalGameEnd (humanPlayer, AI) {
    document.querySelectorAll('.modal-end')[0].style.display = 'flex';

    if (humanPlayer.score > AI.score) {
        document.getElementById('final-score').insertAdjacentText('afterbegin', `Human wins!`);
    } else {
        document.getElementById('final-score').insertAdjacentText('afterbegin', `AI wins!`);
    }
}

function launch() {
    const game = new Game(difficulty);
    const AI = new Player(0);
    const humanPlayer = new Player(0);
    gameProcess(game,humanPlayer,AI);
}

function gameProcess(game,AI,humanPlayer) {
    let playerClicked = false;
    let randomCellArray = randomize(100);
    let passedRepeatChecks = game.checkRepeats(randomCellArray);
    let randomCell = document.querySelectorAll('.cell') [passedRepeatChecks];

    randomCell.style.backgroundColor = 'blue';
    randomCell.addEventListener('click', () => {
        if (randomCell.id !== 'false') {
            randomCell.id = 'false';
            randomCell.style.backgroundColor = 'green';
            playerClicked = 'true';
            humanPlayer.score++;
            humanScoreCount.innerText = humanPlayer.score;
            game.score++;
            if (game.score === 50) {
                modalGameEnd(humanPlayer,AI);
            } else {
                gameProcess(game,AI,humanPlayer);
            }
        }
    });

    setTimeout(() => {
        if (playerClicked === false) {
            randomCell.id = 'false';
            randomCell.style.backgroundColor = 'red';
            AI.score++;
            AIScoreCount.innerText = AI.score;
            game.score++;
            if (game.score === 50) {
                modalGameEnd(humanPlayer,AI);
            } else {
                gameProcess(game,AI,humanPlayer);
            }
        }
    }, game.difficulty);
}


